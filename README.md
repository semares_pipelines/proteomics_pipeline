# Proteomics pipeline

## Abot the pipeline

This is a pipeline to process proteomics data. The pipeline execution is managed by [nextflow](https://www.nextflow.io/); the [docker](https://www.docker.com/) container is [in the container repository](registry.gitlab.com/semares_pipelines/proteomics_pipeline/semares_proteomics:1.1.0). The pipeline is written on [python](https://www.python.org/) and uses [R](https://www.r-project.org/) modules via [rpy2](https://www.r-project.org/). The main R module is [limma](https://bioconductor.org/packages/release/bioc/html/limma.html), which still does not have good Python analogs.

The main steps of the pipeline are:
- Replicate merge
- Normalization
- Imputation
- PCA plot
- Differential abundance for filtering
- Differential abundance
- SynGO annotation
- Excel report

![pipeline flowchart](docs/proteomics_pipeline_diagram.jpg "Pipeline flowchart")

## Pipeline parameters

### Input/output
- `--input_file` path of the input file
- `--metadata_file` path of the metadata file
- `--output_folder` path of the output folder 

### General
- `--imputation_method` the impotation method to be used (zero, lod, norm, knn)
- `--log_transform` true/false if log transformation is necessary for imputation and differential abundance
- `--pca` true/false if pca should be calculated and graph produced
- `--replicate_merge` true/false if replicates should be merged
- `--replicate_suffix` regexp to be used to detect suffix of the column name with replicates (e.g. can be _T\d+ In this case in Semares You should provide _T\\\\d+, in bash _T\\\\\\\\d+ ) 

### Filtering 
- `--filter_norm` true/false if filtering should be based on normalized data
- `--filter_pmod` true/false if to filter data using pmod
- `--filter_pmod` true/false if to filter data using pord
- `--filter_pthr` threshold used for filtering by pmod or pord
- `--filter_logfc` true/false if to filter data using logfc
- `--filter_logfc_thr` threshold used for filtering by logfc
- `--filter_metadata_field` metadata field used for filtering differential abundance (if provided, it is metadata filed, and `filter_group1` and `filter_group2` are metadata values for grouping; if not provided, `filter_group1` and `filter_group2` are comma-separated lists of columns for the groups)
- `--filter_group1` group1 for filtering differential abundance (value or comma-separated list of columns, s. `filter_metadata_field`)
- `--filter_group2` group2 for filtering differential abundance (value or comma-separated list of columns, s. `filter_metadata_field`) 

### Differential abundance 
- `--diff_abundance` true/false if differential abundance should be performed
- `--da_norm` true/false if differential abundance should be based on normalized data
- `--da_pmod` true/false if to filter data using pmod after differential abundance
- `--da_pmod` true/false if to filter data using pord after differential abundance
- `--da_pthr` threshold used for filtering by pmod or pord after differential abundance
- `--da_logfc` true/false if to filter data using logfc after differential abundance
- `--da_logfc_thr` threshold used for filtering by logfc after differential abundance
- `--da_metadata_field` metadata field used for differential abundance (if provided, it is metadata filed, and `da_group1` and `da_group2` are metadata values for grouping; if not provided, `da_group1` and `da_group2` are comma-separated lists of columns for the groups)
- `--da_group1` group1 for differential abundance (value or comma-separated list of columns, s. `da_metadata_field`)
- `--da_group2` group2 for differential abundance (value or comma-separated list of columns, s. `da_metadata_field`) 

### Syngo annotation
- `--syngo_annotate` true/false if to perform syngo annotation
- `--syngo_protein_cc` syngo protein annotation file
- `--syngo_ontology` syngo ontology hierarchy file
 
### Protein name mapping
- `--protein_map` dictionary file to map protein ids to names
- `--protein_map_source_field` source field to use for mapping
- `--protein_map_dest_field` destination field to use for mapping

## Standalone usage

For standalone usage, install `nextflow` and run the pipeline:

```
nextflow run https://gitlab.com/semares_pipelines/proteomics_pipeline.git --input_file <input file> --output_folder <output_folder> <other parameters>
```
An example executable script `run_pipeline.sh` is located in the root folder of the repository. Test data is located in the folder `test_data`.

## Semares usage

To use the pipeline with Semares, add pipeline configuration to the `pipelines.yaml` file. An example configuration is in the file `semares.yaml`.
