import pandas as pd

def add_pnas(data, args):
    global pnas_map
    if args.pnas_annotate=="false":
        return data
    if pnas_map is None:
        load_pnas(args)
    new_res = []
    for index, rows in data.iterrows():
        rec = {}
        pnas_rat_protein_col_d = "pnas_rat_protein_mapped"
        pnas_p2_rank_col_d = "pnas_p2_rank"
        pnas_sv_rank_col_d = "pnas_sv_rank"
        pnas_takamori_col_d = "pnas_takamori"
        if index in pnas_map:
            for pnas_ind, pnas_row in enumerate(pnas_map[index]):
                 pnas_rat_protein_col = pnas_rat_protein_col_d
                 pnas_p2_rank_col = pnas_p2_rank_col_d
                 pnas_sv_rank_col = pnas_sv_rank_col_d
                 pnas_takamori_col = pnas_takamori_col_d
                 if pnas_ind>0:
                     pnas_rat_protein_col+="_"+str(pnas_ind)
                     pnas_p2_rank_col+="_"+str(pnas_ind)
                     pnas_sv_rank_col+="_"+str(pnas_ind)
                     pnas_takamori_col+="_"+str(pnas_ind)
                 rec[pnas_rat_protein_col] = pnas_row["rat_protein"]
                 rec[pnas_p2_rank_col] = pnas_row["p2_rank"]
                 rec[pnas_sv_rank_col] = pnas_row["sv_rank"]
                 rec[pnas_takamori_col] = pnas_row["takamori"]
        else:
            rec[pnas_rat_protein_col_d] = None
            rec[pnas_p2_rank_col_d] = None
            rec[pnas_sv_rank_col_d] = None
            rec[pnas_takamori_col_d] = None       
        new_res.append(rec)
    new_res_df = pd.DataFrame(new_res, index = data.index)
    res_df = data.merge(new_res_df, left_index=True, right_index=True)
    return res_df
        
def load_pnas(args):
    global pnas_map
    
    pnas_df = pd.read_csv(args.pnas_data, sep = "\t")
    pnas_map = {}
    for index, row in pnas_df.iterrows():
        mouse_prot_arr = row["mouse_proteins"].split(";")
        for prot in mouse_prot_arr:
            if prot not in pnas_map:
                pnas_map[prot] = []
            pnas_map[prot].append(row)

pnas_map = None    