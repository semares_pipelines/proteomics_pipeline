import pandas as pd
import numpy as np
import math
import plotly.graph_objects as go
import os
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt

def pca(data_df, group1, group2, legend_name = 'groups', group1_name = "group1", group2_name = "group2", log_transform = True):

    gr1gr2 = group1 + group2
    data_df_filt = data_df[gr1gr2]
    if log_transform:
        data_df_filt = data_df_filt.applymap(lambda x: math.log2(x))

    pca = PCA(n_components = 2)
    components = pca.fit_transform(data_df_filt.transpose())
    components_df = pd.DataFrame(components, columns=["pca 1","pca 2"], index = data_df_filt.columns)
    components_df[legend_name] = components_df.index.map(lambda x: group1_name if x in group1 else group2_name)

    return components_df

def pca_all(data_df, log_transform = True):

    data_df_filt = data_df.copy()
    if log_transform:
        data_df_filt = data_df_filt.applymap(lambda x: math.log2(x))

    pca = PCA(n_components = 2)
    components = pca.fit_transform(data_df_filt.transpose())
    components_df = pd.DataFrame(components, columns=["pca 1","pca 2"], index = data_df_filt.columns)
    components_df.index = data_df_filt.columns

    return components_df

def pca_plot(res_pca, group1, group2, legend_name = 'groups', group1_name = "group1", group2_name = "group2"):

    res_gr1 = res_pca.loc[res_pca.index.isin(group1)]
    res_gr2 = res_pca.loc[res_pca.index.isin(group2)]
    fig1 = go.Figure()
    trace1 = go.Scatter(
     x=res_gr1["pca 1"],
     y=res_gr1["pca 2"],
     mode='markers+text',
     name=group1_name,
     text=list(res_gr1.index),
     textposition="top center",
     line = dict(color='red'),
    )
    trace2 = go.Scatter(
     x=res_gr2["pca 1"],
     y=res_gr2["pca 2"],
     mode='markers+text',
     name=group2_name,
     text=list(res_gr2.index),
     line = dict(color='blue'),
     textposition="top center"

    )

    fig1.update_traces(textposition='top center')
    fig1.add_trace(trace1)
    fig1.add_trace(trace2)
    fig1.update_xaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')
    fig1.update_yaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')

    fig1.update_layout(
        title="PCA",
        xaxis_title="PCA 1",
        yaxis_title="PCA 2",
        legend_title=legend_name,
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)'

    )
    
    fig1.update_layout(
    title={
        'x': 0.5,
        'y': 0.95,
        'xanchor': 'center',
        'yanchor': 'top',
        'font':{
           'size': 30
        }})

    return fig1



def pca_plot_all(res_pca, group_map):

    fig1 = go.Figure()
    return fig1
    
    group_map_rev_map = {}
    for key in group_map:
        val = group_map[key]
        if val not in group_map_rev_map:
            group_map_rev_map[val] = []
        group_map_rev_map[val].append(key)
            
    for gr in group_map_rev_map:
        x = res_pca.loc[group_map_rev_map[gr],"pca 1"]
        y = res_pca.loc[group_map_rev_map[gr],"pca 2"]
        cur_trace = go.Scatter(
         x=x,
         y=y,
         mode='markers+text',
         name=gr,
#         text=list(res_gr1.index),
         textposition="top center",
#         line = dict(color='red'),
        )
 
        fig1.update_traces(textposition='top center')
        fig1.add_trace(cur_trace)
        
    fig1.update_xaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')
    fig1.update_yaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')

    fig1.update_layout(
        title="PCA",
        xaxis_title="PCA 1",
        yaxis_title="PCA 2",
        legend_title=legend_name,
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)'

    )
    
    fig1.update_layout(
    title={
        'x': 0.5,
        'y': 0.95,
        'xanchor': 'center',
        'yanchor': 'top',
        'font':{
           'size': 30
        }})

    return fig1

def pca_plot_all_matplotlib(res_pca, group_field):
   
    fig, ax = plt.subplots(nrows=1)
    levels, categories = pd.factorize(res_pca[group_field])
    color_map = {c: plt.cm.tab10(i) for i,c in enumerate(categories)}
    colors = [plt.cm.tab10(i) for i in levels]
    print(color_map)
    print(colors)
    
    for gr in categories:
       cur_pca = res_pca[res_pca[group_field]==gr]
       sc = ax.scatter(x = cur_pca["pca 1"], y = cur_pca["pca 2"], c=color_map[gr], label=gr)
    ax.legend()
    fig.suptitle("PCA")
    return fig


