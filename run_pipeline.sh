#docker run -it -v $PWD:$PWD semares/proteomics bash -c "$PWD/bin/pipeline.py --input_file $PWD/test_data/696637.csv --group1 NT_S1,NT_Treat_S1,M13_S1,M13_Treat_S1 --group2 vglut1_S1,vglut1_Treat_S1,TID_S1,TID_Treat_S1 --imputation_method zero --output_folder $PWD/test_output"

#./nextflow run main.nf --input_file $PWD/test_data/696637.csv --group1 NT_S1,NT_Treat_S1,M13_S1,M13_Treat_S1 --group2 vglut1_S1,vglut1_Treat_S1,TID_S1,TID_Treat_S1 --imputation_method knn --output_folder $PWD/test_output --syngo_annotate 1 --syngo_protein_cc $PWD/test_data/syngo_proteins_cc_pre_postsynaptic.csv --syngo_ontology $PWD/test_data/syngo_ontology.csv
#./nextflow run main.nf --pca true --diff_abundance true --p_thr 1 --logfc_thr 1 --filter_pmod false --filter_pord  true --filter_logfc true  --log_transform true --input_file $PWD/test_data/696637.csv --metadata_file $PWD/test_data/metadata.json --metadata_field disease --group1 control --group2 treat --imputation_method knn --output_folder $PWD/test_output --syngo_annotate true --syngo_protein_cc $PWD/test_data/syngo_proteins_cc_pre_postsynaptic.csv --syngo_ontology $PWD/test_data/syngo_ontology.csv

docker run -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock nextflow/nextflow /bin/bash -c "cd $PWD && nextflow run main.nf \\
          --input_file $PWD/test_data/230823_thomosyn.txt \\
          --metadata_file $PWD/test_data/metadata_20230823_TomosynTID.json \\
          --output_folder $PWD/test_output \\
          --imputation_method norm \\
          --log_transform true \\
          --pca true \\
          --replicate_merge false \\
          --replicate_suffix '_T\\\\\\d+' \\
          --filter_na true \\
          --filter_na_field 'condition' \\
          --filter_na_group 'Tomosyn-TID' \\
          --filter_na_fraction 0.5 \\
          --filter_norm false \\
          --filter_pmod true \\
          --filter_pord false \\
          --filter_logfc true \\
          --filter_metadata_field condition\\
          --filter_group1 NT \\
          --filter_group2 'Tomosyn-TID' \\
          --filter_p_thr 1.3 \\
          --filter_logfc_thr 1 \\
          --diff_abundance true \\
          --da_norm true \\
          --da_filter_pmod true \\
          --da_filter_pord false \\
          --da_filter_logfc true \\
          --da_metadata_field 'condition'\\
          --da_group1 'cytosolic-TID' \\
          --da_group2 'Tomosyn-TID' \\
          --da_p_thr 1.3 \\
          --da_logfc_thr 1 \\
          --syngo_annotate true \\
          --syngo_protein_cc $PWD/test_data/syngo_proteins_cc_pre_postsynaptic.csv \\
          --syngo_ontology $PWD/test_data/syngo_ontology.csv \\
          --protein_map $PWD/test_data/uniprot_mouse_proteins_reviewed.tsv \\
          --protein_map_source_field Entry \\
          --protein_map_dest_field 'Entry Name'
          "
