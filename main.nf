def processed_params = [:]
for (param in params){
  processed_params[param.key] = param.value
}

file_params = ["input_file", "metadata_file", "output_folder", "syngo_protein_cc", "syngo_ontology", "protein_map"]
for (param in file_params){
  processed_params[param] = processed_params[param] ? file(processed_params[param]) : ""
}

default_string_params = ["filter_metadata_field", "da_metadata_field"]
for (param in default_string_params){
  processed_params[param] = (processed_params.containsKey(param) && (processed_params[param] != true)) ? processed_params[param] : ""
}

process proteomics {
    container 'registry.gitlab.com/semares_pipelines/proteomics_pipeline/semares_proteomics:1.2.0'
    publishDir "${processed_params['output_folder']}", mode: 'copy', saveAs: { fn -> fn.substring(fn.lastIndexOf('/')+1) }

    input:
       val processed_params

    output:
       file "output_folder/*"

    """
        pipeline.py \\
          --input_file '${processed_params["input_file"]}' \\
          --metadata_file '${processed_params["metadata_file"]}' \\
          --output_folder "output_folder" \\
          --imputation_method '${processed_params["imputation_method"]}' \\
          --log_transform '${processed_params["log_transform"]}' \\
          --pca '${processed_params["pca"]}' \\
          --replicate_merge '${processed_params["replicate_merge"]}' \\
          --replicate_suffix '${processed_params["replicate_suffix"]}' \\
          --filter_na '${processed_params["filter_na"]}' \\
          --filter_na_field '${processed_params["filter_na_field"]}' \\
          --filter_na_group '${processed_params["filter_na_group"]}' \\
          --filter_na_fraction '${processed_params["filter_na_fraction"]}' \\
          --filter_norm '${processed_params["filter_norm"]}' \\
          --filter_pmod '${processed_params["filter_pmod"]}' \\
          --filter_pord '${processed_params["filter_pord"]}' \\
          --filter_logfc '${processed_params["filter_logfc"]}' \\
          --filter_metadata_field '${processed_params["filter_metadata_field"]}' \\
          --filter_group1 '${processed_params["filter_group1"]}' \\
          --filter_group2 '${processed_params["filter_group2"]}' \\
          --filter_p_thr '${processed_params["filter_p_thr"]}' \\
          --filter_logfc_thr '${processed_params["filter_logfc_thr"]}' \\
          --filter_logfc_two_sided '${processed_params["filter_logfc_two_sided"]}' \\
          --diff_abundance '${processed_params["diff_abundance"]}' \\
          --da_norm '${processed_params["da_norm"]}' \\
          --da_filter_pmod '${processed_params["da_filter_pmod"]}' \\
          --da_filter_pord '${processed_params["da_filter_pord"]}' \\
          --da_filter_logfc '${processed_params["da_filter_logfc"]}' \\
          --da_metadata_field '${processed_params["da_metadata_field"]}' \\
          --da_group1 '${processed_params["da_group1"]}' \\
          --da_group2 '${processed_params["da_group2"]}' \\
          --da_p_thr '${processed_params["da_p_thr"]}' \\
          --da_logfc_thr '${processed_params["da_logfc_thr"]}' \\
          --da_logfc_two_sided '${processed_params["da_logfc_two_sided"]}' \\
          --pnas_annotate '${processed_params["pnas_annotate"]}' \\
          --pnas_data '${processed_params["pnas_data"]}' \\
          --lyso_annotate '${processed_params["lyso_annotate"]}' \\
          --lyso_data '${processed_params["lyso_data"]}' \\
          --lyso_sheet '${processed_params["lyso_sheet"]}' \\
          --endo_annotate '${processed_params["endo_annotate"]}' \\
          --endo_data '${processed_params["endo_data"]}' \\
          --endo_sheet '${processed_params["endo_sheet"]}' \\
          --endo_manual_sheet '${processed_params["endo_manual_sheet"]}' \\
          --syngo_annotate '${processed_params["syngo_annotate"]}' \\
          --syngo_protein_cc '${processed_params["syngo_protein_cc"]}' \\
          --syngo_ontology '${processed_params["syngo_ontology"]}' \\
          --ontology_annotate '${processed_params["ontology_annotate"]}' \\
          --ontology_sets '${processed_params["ontology_sets"]}' \\
          --ontology_p_threshold '${processed_params["ontology_p_threshold"]}' \\
          --protein_map_file '${processed_params["protein_map"]}' \\
          --protein_map_source_field "${processed_params['protein_map_source_field']}" \\
          --protein_map_dest_field "${processed_params['protein_map_dest_field']}" \\
          --protein_map_gene_field "${processed_params['protein_map_gene_field']}"

    """
}

workflow {
   proteomics(processed_params)
}